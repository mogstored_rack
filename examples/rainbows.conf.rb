# A heavily annotated configuration file for running Rainbows! with
# MogstoredRack.  There is not much configuration here.

# be sure to configure stderr_path for debugging/diagnosis
# You can reopen this file after log rotation using the SIGUSR1
stderr_path "/path/to/log/stderr.log"

# You still need the Perl mogstored listening on the management port,
# but this is the TCP port your HTTP server listens on:
listen 7500

# this config option breaks the current Rack spec, but eliminates
# double-buffering on uploads which can greatly improve PUT performance
# and halve disk usage during uploads.
rewindable_input false

Rainbows! do
  # this defaults to 1 megabyte in Rainbows!, so you need to increase it
  # if you plan to store larger files in MogileFS.   This setting is in
  # bytes, so 5 gigabytes is: 5 * 1024 * 1024 * 1024
  # +nil+ means there is no limit
  client_max_body_size nil

  # native threads under Ruby 1.9.3 perform very well for I/O and do a
  # good job of keeping disks busy, especially for GET requests with
  # large files.  The ThreadSpawn and ThreadPool concurrency models in
  # Rainbows! allow us to take advantage of the "rewindable_input false"
  # setting we set above.
  if RUBY_PLATFORM =~ /linux/
    # If running Linux, the "sleepy_penguin" RubyGem provides access to a
    # thread-safe epoll wrapper that enables the use of blocking accept()
    # to avoid a thundering herd with multiple processes as well as
    # inexpensive keepalive.
    require "sleepy_penguin"
    use :XEpollThreadSpawn
  else
    use :ThreadSpawn
  end

  # by default, the Rainbows! keepalive is only 5s.  While the
  # mogilefsd monitor currently checks hosts every 2.5 seconds,
  # installations with many devices/hosts can take longer to cycle
  # through all devices as device monitoring is currently sequential.
  keepalive_timeout 60

  # You may need to tweak ulimit to increase worker_connections beyond 1024
  # You can set this permanently in many Linux distros via editing
  # /etc/security/limits.conf
  #
  # Like nginx, our total number of connections is actually
  #   worker_processes * worker_connections
  worker_connections 10000
end

# path to the pid file, useful for init scripts
pid "/path/to/run/mogstored_rack.pid"

# Rainbows! can be multi-process as well as multi-threaded/evented.
# Increase or decrease this as you wish to use multiple CPU cores,
# get around worker_connections limits, or improve concurrency for
# HEAD requests and/or serving small files.
worker_processes 2

# If you're on Ruby 1.9.2 or earlier, using the "sendfile" RubyGem is
# recommended to work around IO.copy_stream bugs.  Ruby 1.9.3 or later
# is recommended.
require "sendfile" if RUBY_VERSION <= "1.9.2"

# The documentation for
# {Rainbows!}[http://rainbows.rubyforge.org/Rainbows/Configurator.html]
# and {Unicorn}[http://unicorn.bogomips.org/Unicorn/Configurator.html]
# will provide full explanations for everything here.
#
# As usual, feel free to send plain-text email to the appropriate mailing
# list or {Eric Wong}[mailto:normalperson@yhbt.net] directly if you have
# questions.
