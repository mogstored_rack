# -*- encoding: binary -*-
ENV["VERSION"] or abort "VERSION= must be specified"
manifest = File.readlines('.manifest').map! { |x| x.chomp! }
require 'wrongdoc'
extend Wrongdoc::Gemspec
name, summary, title = readme_metadata

Gem::Specification.new do |s|
  s.name = %q{mogstored_rack}
  s.version = ENV["VERSION"].dup

  s.authors = ["Eric Wong"]
  s.date = Time.now.utc.strftime('%Y-%m-%d')
  s.description = readme_description
  s.email = %q{normalperson@yhbt.net}
  s.extra_rdoc_files = extra_rdoc_files(manifest)
  s.files = manifest
  s.homepage = Wrongdoc.config[:rdoc_url]
  s.summary = summary
  s.rdoc_options = rdoc_options
  s.rubyforge_project = %q{rainbows}
  s.test_files = Dir["test/test_*.rb"]

  s.add_dependency("rack", ["~> 1.3"])

  s.add_development_dependency('wrongdoc', "~> 1.6")
  # s.licenses = %w(GPL3) # accessor not compatible with older RubyGems
end
