# -*- encoding: binary -*-
require "test/unit"
require "tmpdir"
require "fileutils"
require "mogstored_rack"

class TestUnitMogstoredRack < Test::Unit::TestCase
  attr_reader :req

  def setup
    @docroot = Dir.mktmpdir(["mogstored_rack", ".docroot"])
  end

  def test_defaults
    req = Rack::MockRequest.new(MogstoredRack.new(@docroot))
    all_methods(req)
  end

  def test_fsync_true
    req = Rack::MockRequest.new(MogstoredRack.new(@docroot, :fsync=>true))
    all_methods(req)
  end

  def all_methods(req)
    assert_equal 200, req.get("/").status # needed to pass "mogadm check"
    assert ! File.directory?("#@docroot/dev666")

    r = req.request("PUT", "/dev666/666.fid", :input => StringIO.new("AAAA"))
    assert_equal 403, r.status

    assert ! File.directory?("#@docroot/dev666")

    assert_equal 204, req.request("MKCOL", "/dev666").status
    assert File.directory?("#@docroot/dev666")

    io = StringIO.new("HELLO")
    r = req.request("PUT", "/dev666/666.fid", :input => io)
    assert_equal 201, r.status
    assert_equal "HELLO", IO.read("#@docroot/dev666/666.fid")

    # invalid MD5
    io = StringIO.new("WORLD")
    md5 = [ Digest::MD5.new.digest ].pack("m").strip!
    opts = { :input => io, "HTTP_CONTENT_MD5" => md5 }
    r = req.request("PUT", "/dev666/666.fid", opts)
    assert_equal 400, r.status
    assert ! File.exist?("#@docroot/dev666/666.fid")

    # valid MD5
    io = StringIO.new("VALID")
    md5 = [ Digest::MD5.digest("VALID") ].pack("m").strip!
    opts = { :input => io, "HTTP_CONTENT_MD5" => md5 }
    r = req.request("PUT", "/dev666/666.fid", opts)
    assert_equal 201, r.status
    assert_equal "VALID", IO.read("#@docroot/dev666/666.fid")

    r = req.request("GET", "/dev666/666.fid")
    assert_equal 200, r.status
    assert_equal "VALID", r.body

    r = req.request("DELETE", "/dev666/666.fid")
    assert_equal 204, r.status
    assert ! File.exist?("#@docroot/dev666/666.fid")
  end

  def teardown
    FileUtils.rmtree(@docroot)
  end
end
