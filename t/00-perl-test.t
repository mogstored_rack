use MogileFS::Client;
use Test::More;
my $domain = $ENV{MOG_TEST_DOMAIN} or die "MOG_TEST_DOMAIN not defined";
my $trackers = $ENV{MOG_TEST_TRACKERS} or die "MOG_TEST_TRACKERS not defined";
my @trackers = split(/,/, $trackers);

my $mogc = MogileFS::Client->new(hosts => \@trackers, domain=>$domain);

my $fh = $mogc->new_file("testkey", "");
ok($fh, "created new file handle");
for my $i (0..10000) { print $fh "$i\n" or die "failed to write $!\n" }
ok(close($fh), "closed file handle");
my $expect = $mogc->get_file_data("testkey");


$fh = $mogc->new_file("testlargekey", "", undef, { largefile => 1});
ok($fh, "created new largefile handle");
for my $i (0..10000) { print $fh "$i\n" or die "failed to write $!\n" }
ok(close($fh), "closed largefile handle");
my $actual = $mogc->get_file_data("testlargekey");
is($$actual, $$expect, "largefile PUT succeeded");

done_testing();
